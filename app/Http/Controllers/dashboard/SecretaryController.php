<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;
class SecretaryController extends Controller
{

    public function index()
    {

        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

        $secretaries = User::where('id' , '!=' , Auth::user()->id)->get();
        return view('dashboard.secretary.index' , compact('secretaries'));
    } // end function index

    public function create()
    {
        return view('dashboard.secretary.create');
    } // end function create

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password|min:6',
        ]);

        $secretary = new User();
        $secretary->name = $request->get('name');
        $secretary->email = $request->get('email');
        $secretary->password = Hash::make($request->get('password'));

        $secretary->save();

        return redirect()->route('secretary.index')->with('success' , trans('main.Add Is Successfully'));


    } // end function store

    public function login()
    {

        // session message errors
        if (session()->has('error')) {
            alert()->error(trans('main.Oops'),session()->get('error'));
        }

    	return view('dashboard.login');
    } // end function login

    public function loginSecretary(Request $request)
    {
    	if (auth()->attempt(request(['email' , 'password'])) == false) {
    		return redirect()->back()->with('error', trans('main.Email Or Password Is Not Found'));
    	}

    	return redirect()->route('welcome')->with('success' , trans('main.Login is Successfully'));

    } // end function login secretary

    public function logout()
    {

        auth()->logout();

        return redirect()->route('login');

    } // end function logout
}
