@extends('dashboard.layout.app')
@section('title')
    All Patients
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">


                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-0">{{ trans('main.Search') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{ route('search') }}" method="get" class="form form-horizontal">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <input type="text" id="name" class="form-control" name="search" placeholder="{{ trans('main.Code Patient') }}">
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                    <div class="col-md-5">
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> {{ trans('main.Search') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- Dashboard Analytics end -->                

                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-0">{{ trans('main.All Patients') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="table-responsive mt-1">
                                        <table class="table zero-configuration" id="{{ app()->getLocale() == 'ar' ? 'table_patient' : '' }}">
                                            <thead>
                                                <tr>

                                                    <th>{{ trans('main.Code') }}</th>

                                                    <th>{{ trans('main.Patient Name') }}</th>
                                                    <th>{{ trans('main.Address') }}</th>
                                                    <th>{{ trans('main.Phone') }}</th>
                                                    <th>{{ trans('main.Age') }}</th>
                                                    <th>{{ trans('main.View') }}</th>
                                                    <th>{{ trans('main.Add Booking') }}</th>
                                                    <th>{{ trans('main.Edit') }}</th>
                                                    <th>{{ trans('main.Delete') }}</th>
                                           
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($patients as $patient)
                                                <tr>
                                                    <td>#{{ $patient->code }}</td>
                                                    <td>{{ $patient->name }}</td>
                                                    <td>{{ $patient->address }}</td>
                                                    <td>{{ $patient->phone }}</td>
                                                    <td>{{ $patient->age }}</td>
                                                    <td><a href="{{ route('view' , $patient->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>{{ trans('main.View') }}</a></td>
                                                    <td><a href="{{ route('booking.patient' , $patient->id) }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>{{ trans('main.Add Booking') }}</a></td>
                                                    <td><a href="{{ route('patients.edit' , $patient->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> {{ trans('main.Edit') }}</a></td>
                                                    <td>
                                                        <form action="{{ route('patients.destroy' , $patient->id) }}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> {{ trans('main.Delete') }}</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- Dashboard Analytics end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

@section('scripts')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ url('app-assets/js/scripts/datatables/datatable.js') }}"></script>
    <!-- END: Page JS-->

    <script type="text/javascript">
                // translate datatable
            $('#table_patient').DataTable({
                 
                 "language": 
                        {
                            "sProcessing": "جارٍ التحميل...",
                            "sLengthMenu": "أظهر _MENU_ مدخلات",
                            "sZeroRecords": "لم يعثر على أية سجلات",
                            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                            "sInfoPostFix": "",
                            "sSearch": "ابحث:",
                            "sUrl": "",
                            "oPaginate": {
                                "sFirst": "الأول",
                                "sPrevious": "السابق",
                                "sNext": "التالي",
                                "sLast": "الأخير"
                            }
                        } // end language

                }); // end datatable       
    </script>         

@endsection