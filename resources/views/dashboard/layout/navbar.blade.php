    <!-- BEGIN: Header-->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@700&display=swap" rel="stylesheet">

    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">

                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">

                    </div>

                    <ul class="nav navbar-nav float-right" style="font-family: 'Cairo', sans-serif;">

                        <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language">@if(app()->getLocale() == 'ar') عربي  @else English @endif</span></a>

                            <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                                <a class="dropdown-item" href="/dashboard/locale/en" data-language="en"><i class="flag-icon flag-icon-us"></i> {{ trans('main.English') }}</a>
                                <a class="dropdown-item" href="/dashboard/locale/ar" data-language="ar"><i class="flag-icon flag-icon-eg"></i> {{ trans('main.Arabic') }}</a>
                            </div>
                        </li>

                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{ Auth::user()->name }}</span></div>
                                <span>
                                    @if(Auth::user()->role == 'Doctor')
                                    <img class="round" src="{{ url('images/doctor.jpg') }}" alt="avatar" height="40" width="40">
                                    @else
                                    <img class="round" src="{{ url('images/secretary.jpg') }}" alt="avatar" height="40" width="40">
                                    @endif
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="{{ route('logout') }}"><i class="feather icon-power"></i> {{ trans('main.Logout') }}</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- END: Header-->
