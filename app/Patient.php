<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{

    protected $fillable = [ 'code' , 'name' , 'address' , 'phone' , 'age' ];

    public function bookings()
    {
    	return $this->hasMany(Booking::class);
    }

    public function detect()
    {
    	return $this->hasMany(Detect::class);
    }
}
