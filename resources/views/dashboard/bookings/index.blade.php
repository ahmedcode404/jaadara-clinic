@extends('dashboard.layout.app')
@section('title')
    All Bookings
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">

                @include('dashboard.layout.messages')

                <!-- BEGIN: bookings -->
                <section id="dashboard-analytics">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-0">{{ trans('main.All Bookings') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="table-responsive mt-1">
                                        <table class="table zero-configuration" id="{{ app()->getLocale() == 'ar' ? 'table_booking' : '' }}">
                                            <thead>
                                                <tr>

                                                    <th>
                                                        {{ trans('main.Patient Name') }}
                                                    </th>
                                                    <th>{{ trans('main.Date') }}</th>
                                                    <th>{{ trans('main.Price') }}</th>
                                                    <th>{{ trans('main.Price Plus Booking') }}</th>
                                                    <th>{{ trans('main.Type') }}</th>
                                                    <th>{{ trans('main.Edit') }}</th>
                                                    <th>{{ trans('main.Delete') }}</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($bookings as $booking)
                                                <tr>
                                                    <td>

                                                        {{ $booking->patient->name }}

                                                    </td>
                                                    <td>{{ $booking->date }}</td>
                                                    <td>{{ $booking->price }}</td>
                                                    <td>
                                                        @if($booking->price_plus)
                                                            {{ $booking->price_plus }}
                                                        @else
                                                            0
                                                        @endif
                                                    </td>
                                                    <td>{{ $booking->type }}</td>

                                                    <td><a href="{{ route('bookings.edit' , $booking->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a></td>

                                                    <td>
                                                        <form action="{{ route('bookings.destroy' , $booking->id) }}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                                        </form>
                                                    </td>

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- END: bookings -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

@section('scripts')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ url('app-assets/js/scripts/datatables/datatable.js') }}"></script>
    <!-- END: Page JS-->

    <script type="text/javascript">
                // translate datatable
            $('#table_booking').DataTable({

                 "language":
                        {
                            "sProcessing": "جارٍ التحميل...",
                            "sLengthMenu": "أظهر _MENU_ مدخلات",
                            "sZeroRecords": "لم يعثر على أية سجلات",
                            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                            "sInfoPostFix": "",
                            "sSearch": "ابحث:",
                            "sUrl": "",
                            "oPaginate": {
                                "sFirst": "الأول",
                                "sPrevious": "السابق",
                                "sNext": "التالي",
                                "sLast": "الأخير"
                            }
                        } // end language

                }); // end datatable
    </script>

@endsection
