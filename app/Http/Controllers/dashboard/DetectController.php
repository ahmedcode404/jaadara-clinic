<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Booking;
use App\Detect;
use App\Patient;
use App\Image;
use DB;
class DetectController extends Controller
{

    public function index()
    {

        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

        // date today from carbon
        $date = Carbon::today();

        $detects = Booking::where('status' , 0)->whereDay('date' , $date)->orderBy('date' , 'ASC')->get();

        return view('dashboard.detects.index' , compact('detects'));

    } // end function all detect

    public function detect(Booking $booking)
    {
        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

    	$latestDetect = Detect::where('patient_id' , $booking->patient->id)->latest()->first();
    	return view('dashboard.detects.create' , compact('booking' , 'latestDetect'));

    } // end function index

    public function detectPatient(Patient $patient , Booking $booking)
    {
        $detectPatient = Detect::where('patient_id' , $patient->id)->get();
        return view('dashboard.detects.detectPatient' , compact('booking' , 'detectPatient' , 'patient'));

    } // end function detect user

    public function detectComplete(Detect $detect)
    {

        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

        // date today from carbon
        $date = Carbon::today();

        // get detect complete day
        $detectComplete = Booking::where('status' , 1)->whereDay('date' , $date)->orderBy('date' , 'ASC')->get();

        // get price detect complete day
        $priceDetectDay = Booking::where('status' , 1)->whereDay('date' , $date)->select(

            DB::raw('Day(date) as day'),
            DB::raw('SUM(price) as sum'),
            DB::raw('SUM(price_plus) as sum_plus')

        )->groupBy('day')->get();

        return view('dashboard.detects.detectComplete' , compact('detectComplete' , 'priceDetectDay'));

    } // end function all detect

    public function store(Request $request)
    {

    	$request->validate([
            'diagnosing' => 'required',
            'anitdotes' => 'min:3',
            'other' => 'min:3',
            'rays*' => 'image|mimes:jpeg,png,jpg'
        ]);



    	$detect = new Detect();
        $detect->patient_id = $request->get('patient_id');
    	$detect->booking_id = $request->get('booking_id');
    	$detect->diagnosing = $request->get('diagnosing');
        $detect->anitdotes = $request->get('anitdotes');
    	$detect->other = $request->get('other');
        $detect->save();

        // uploade mulit rays rays
        if ($files = $request->file('rays')) {

                // track folder
                $public = public_path('/images/detects/');

            foreach ($files as $file) {

                // get name rays
                $name = uniqid() . $file->getClientOriginalName();
                // track uploade
                $file->move($public , $name);

                // uploade rays to table images
                $upload = new Image();
                $upload->detect_id = $detect->id;
                $upload->detect_iamges = $name;
    	        $upload->save();

            } // end foreach

        } // end if




        // update booking status from 0 to 1
        $booking = Booking::where('id' , $detect->booking_id)->first();

        $booking->status = 1;

        $booking->update();

    	return redirect()->route('detects')->with('success' , trans('main.Complete Detect Is Successfully'));


    }
}
