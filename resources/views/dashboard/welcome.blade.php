@extends('dashboard.layout.app')
@section('title')
    Welcome
@endsection
<style type="text/css">
    a.disabled{
        pointer-events: none;
        cursor: default;
    }
</style>
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="dashboard-analytics">
                    <div class="row">
                        <div class="col-lg-12 col-md-6 col-12">
                            <img src="{{ url('images/banner.png') }}" width="1027px" height="537px">
                        </div>
                    </div>
                </section>
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="avatar bg-rgba-primary p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-users text-primary font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700 mt-1 mb-25">{{ $patients->count() }}</h2>
                                    <p class="mb-0"><a href="{{ route('patients.index') }}">{{ trans('main.All Patients') }}</a></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="avatar bg-rgba-primary p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-users text-primary font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700 mt-1 mb-25">{{ $booking->count() }}</h2>
                                    <p class="mb-0"><a href="{{ route('bookings.index') }}">{{ trans('main.All Bookings') }}</a></p>
                                </div>
                            </div>
                        </div>



                    </div>
                </section>



                <section id="dashboard-analytics">
                    <div class="row">

                        <div class="col-lg-12 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="bg-rgba-primary p-50 m-0">
                                            <h1>{{ trans('main.Calender') }}</h1>
                                    </div>
                                    <table class="table" style="font-size: 10px;">

                                        <thead>
                                            <tr>

                                                @for($i = 1 ; $i <= 30 ; $i++)

                                                @if(in_array( $i , $arrayBooking))

                                                <td style="border: 1px solid #4f4fb7;background: #cdcdd6">
                                                    <a href="{{ route('booking' ,  $i) }}">

                                                        @if($i < 10)
                                                        0{{ $i }}
                                                        @else
                                                        {{ $i }}
                                                        @endif

                                                    </a>
                                                </td>

                                                @else

                                                <td class="disabled">
                                                    <a href="" class="disabled">

                                                        @if($i < 10)
                                                        0{{ $i }}
                                                        @else
                                                        {{ $i }}
                                                        @endif

                                                    </a>
                                                </td>


                                                @endif
                                                @endfor

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

@section('scripts')

    <!-- BEGIN: Page Vendor JS-->
    <script src="../../../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="../../../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS-->

    <script type="text/javascript">
                // translate datatable
            $('.table_all').DataTable({

                 "language":
                        {
                            "sProcessing": "جارٍ التحميل...",
                            "sLengthMenu": "أظهر _MENU_ مدخلات",
                            "sZeroRecords": "لم يعثر على أية سجلات",
                            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                            "sInfoPostFix": "",
                            "sSearch": "ابحث:",
                            "sUrl": "",
                            "oPaginate": {
                                "sFirst": "الأول",
                                "sPrevious": "السابق",
                                "sNext": "التالي",
                                "sLast": "الأخير"
                            }
                        } // end language

                }); // end datatable
    </script>

@endsection
