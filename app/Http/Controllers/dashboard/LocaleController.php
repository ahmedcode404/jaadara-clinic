<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    public function locale($locale)
    {
    	
    	// change language locale
    	\App::setLocale($locale);
    	
    	session()->put('locale', $locale);

    	return redirect()->back();
    }
}
