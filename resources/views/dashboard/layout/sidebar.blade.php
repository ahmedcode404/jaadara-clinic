    <!-- BEGIN: Main Menu-->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@700&display=swap" rel="stylesheet">

    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="font-family: 'Cairo', sans-serif;
">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('welcome') }}">
                        <div class=""><img src="{{ url('images/logo.png') }}" width="200px" height="200px"></div>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" style="font-family: 'Cairo', sans-serif;margin-top: 170px;
">
                <li class=" nav-item"><a href="{{ route('welcome') }}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">{{ trans('main.Dashboard') }}</span></a>
                </li>
                @if(Auth::user()->role == 'Doctor')
                <li class=" nav-item"><a href="index.html"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">{{ trans('main.Secretary') }}</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('secretary.create') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">{{ trans('main.Add Secretary') }}</span></a>
                        </li>
                        <li><a href="{{ route('secretary.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="eCommerce">{{ trans('main.All Secretary') }}</span></a>
                        </li>
                    </ul>
                </li>
                @endif
                <li class=" nav-item"><a href="index.html"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">{{ trans('main.Patients') }}</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('patients.create') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">{{ trans('main.Add Patient') }}</span></a>
                        </li>
                        <li><a href="{{ route('patients.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="eCommerce">{{ trans('main.All Patients') }}</span></a>
                        </li>
                    </ul>
                </li>

                <li class=" nav-item"><a href="{{ route('bookings.index') }}"><i class="feather icon-clock"></i><span class="menu-title" data-i18n="Dashboard">{{ trans('main.Bookings') }}</span></a>
                </li>

                <li class=" nav-item"><a href="{{ route('detects') }}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">{{ trans('main.All Detects') }}</span></a>
                </li>

                <li class=" nav-item"><a href="{{ route('detect.complete') }}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">{{ trans('main.Detect Complete') }}</span></a>
                </li>

            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->
