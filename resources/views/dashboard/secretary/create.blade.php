@extends('dashboard.layout.app')
@section('title')
    Add Secretary
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @include('dashboard.layout.messages')
                
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ trans('main.Add Secretary') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{ route('secretary.store') }}" method="post" class="form form-horizontal">
                                        	@csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Secretary Name') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" value="{{ old('name') }}" id="name" class="form-control" name="name" placeholder="{{ trans('main.Secretary Name') }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Email') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="email" value="{{ old('email') }}" id="email" class="form-control" name="email" placeholder="{{ trans('main.Email') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Password') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="password" value="{{ old('password') }}" id="password" class="form-control" name="password" placeholder="{{ trans('main.Password') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Confirm Password') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="password" id="confirm_password" value="{{ old('confirm_password') }}" class="form-control" name="confirm_password" placeholder="{{ trans('main.Confirm Password') }}">
                                                            </div>
                                                        </div>
                                                    </div>                                                                                                        

                                                    <div class="col-md-8 offset-md-4">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1"><i class="fa fa-plus"></i> {{ trans('main.Add Secretary') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->            	


            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection