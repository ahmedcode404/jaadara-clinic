<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $fillable = [ 'patient_id' , 'date', 'price' , 'type' , 'price_plus' ];

    public function patient()
    {
    	return $this->belongsTo(Patient::class);
    }

    public function detects()
    {
    	return $this->hasMany(Detect::class);
    }    
}
