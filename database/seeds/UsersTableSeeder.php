<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = \App\User::create([
        	'name' => 'Doctor',
        	'email' => 'doctor@gmail.com',
            'password' => bcrypt('123456'),
        	'role' => 'Doctor',
        ]);
        
    }
}
