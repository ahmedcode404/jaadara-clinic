<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Patient;
use App\Booking;
class BookingsController extends Controller
{
	public function index()
    {

        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

        $date = Carbon::today();


    	$bookings = Booking::where('status' , 0)->where('date' , '>=' , $date)->orderBy('date' , 'ASC')->get();

        //dd($bookings->first());

    	return view('dashboard.bookings.index' , compact('bookings'));

    } // end function index

    public function create(Patient $patient)
    {

        // session message errors
        if (session()->has('error')) {
            alert()->error(trans('main.Oops'),session()->get('error'));
        }

    	return view('dashboard.bookings.create' , compact('patient'));

    } // end function create

    public function store(Request $request , Patient $patient)
    {

    	$request->validate([
    		'date' => 'required|unique:bookings',
    		'price' => 'required',
    		'type' => 'required',
    	]);

    	// condition validate type for enter price plus
    	if ($request->get('type') == 'operation') {

		    	$request->validate([
		    		'price_plus' => 'required',
		    	]);

    	} // end if

        $date = Carbon::today();




    	$booking = new Booking();
    	$booking->patient_id = $request->get('patient_id');
    	$booking->price = $request->get('price');

        if ($request->get('date') <= $date) {
            return redirect()->back()->with('error' , trans('main.Can Not Booking In Date Old'));

         }else {

    	   $booking->date = $request->get('date');

         }
    	$booking->type = $request->get('type');
        $booking->price_plus = $request->get('price_plus');
        $booking->status = 0;
    	$booking->save();


    	return redirect()->route('bookings.index')->with('success' , trans('main.Add Is Successfully'));

    } // end function store

    public function edit(Booking $booking)
    {
        // session message errors
        if (session()->has('error')) {
            alert()->error(trans('main.Oops'),session()->get('error'));
        }

        return view('dashboard.bookings.edit' , compact('booking'));
    } // end function edit

    public function update(Request $request , Booking $booking)
    {

        $date = Carbon::today();

        $request->validate([
            'date' => 'required',
            'price' => 'required',
            'type' => 'required',
        ]);

        // condition validate type for enter price plus
        if ($request->get('type') == 'operation') {

                $request->validate([
                    'price_plus' => 'required',
                ]);

        } // end if

        $booking->patient_id = $request->get('patient_id');

        if ($request->get('date') <= $date) {
            return redirect()->back()->with('error' , trans('main.Can Not Booking In Date Old'));

         }else {

           $booking->date = $request->get('type');

         }

        $booking->type = $request->get('type');
        $booking->price_plus = $request->get('price_plus');

        $booking->save();

        return redirect()->route('bookings.index')->with('success' , trans('main.Edit Is Successfully'));

    } // end function update

    public function destroy(Booking $booking)
    {

        $booking->delete();

        return redirect()->back()->with('success' , trans('main.Delete Is Successfully'));

    } // end function destroy

    public function bookingDay($booking)
    {


            $allBooking = Booking::whereDay('date' ,  $booking)->orderBy('date' , 'ASC')->get();

        return view('dashboard.bookings.booking' , compact('allBooking'));

    }
}
