<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Patient;
use App\Booking;
use App\Detect;
class WelcomeController extends Controller
{
    public function welcome()
    {

        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

        // session message errors
        if (session()->has('error')) {
            alert()->error('Oops',session()->get('error'));
        }



        $date = Carbon::today();


        $arrayBooking = [];

        $bookings = Booking::where('status' , 0)->where('date' , '>=' , $date)->get();

        foreach ($bookings as $booking)
        {

            $dat = new Carbon($booking->date);

            $arrayBooking[] = $dat->day;

        }

        // get booking count
        $booking = Booking::where('status' , 0)->where('date' , '>=' , $date)->get();


    	$patients = Patient::all();
    	$detects = Detect::all();

    	return view('dashboard.welcome' , compact('patients','booking','detects','arrayBooking'));

    }
}
