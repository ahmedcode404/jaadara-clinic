@extends('dashboard.layout.app')
@section('title')
    Search
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">


                <!-- BEGIN: search detect patient -->

                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <!-- condition patient find or no -->

                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ trans('main.All Detect for Patient') }} {{ $patient->name }} </h4>
                                </div>
                                    @if($detectPatient->count())
                                    @foreach($detectPatient as $detect)
                                        
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card-content">
                                                        <div class="card-body">

                                                            <b>{{ trans('main.Diagnosing') }} : </b><span>


                                                                {{ $detect->diagnosing }}


                                                            </span><br><br>
                                                            @if($detect->other)

                                                            <b>{{ trans('main.Anitdotes') }} : </b>
                                                            <span>


                                                                {{ strip_tags($detect->anitdotes) }}
             

                                                            </span>
                                                            @endif
                                                            <br><br>

                                                            @if($detect->images->count())

                                                                <b>{{ trans('main.Rays') }} : </b>
                                                                @foreach($detect->images as $image)
                                                                <span>
                                                                    <img src="{{ url('/images/detects/' . $image->detect_iamges) }}" width="150px" style="margin: 10px;display: inline-block;">
                                                                </span><br><br>

                                                                @endforeach
                                                            @endif    

                                                         @if($detect->other)

                                                            <b>{{ trans('main.Other') }} : </b>
                                                            <span>


                                                                {{ strip_tags($detect->other) }}
             

                                                            </span> 
                                                        @endif                                                                   

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <hr>
                                    @endforeach

                                    @else
                                    <p style="text-align: center;">don't Find Detect For Patient !</p>
                                    @endif
                            </div>
                        </div>
                        
                    </div>
                </section>

                <!-- END: search detect patient -->                




            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

