@extends('dashboard.layout.app')
@section('title')
    Edit Patient
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @include('dashboard.layout.messages')

                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ trans('main.Edit Patient') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{ route('patients.update' , $patient->id) }}" method="post" class="form form-horizontal">
                                        	@csrf
                                            @method('PUT')
                                            <div class="form-body">
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Patient Name') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" id="name" class="form-control" value="{{ $patient->name }}" name="name" placeholder="{{ trans('main.Patient Name') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Address') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" id="address" class="form-control" value="{{ old('address') ?? $patient->address }}" name="address" placeholder="{{ trans('main.Address') }}">
                                                            </div>
                                                        </div>
                                                    </div> 

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Phone') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="number" id="phone" class="form-control"value="{{ $patient->phone }}" name="phone" placeholder="{{ trans('main.Phone') }}">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Age') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="number" id="age" class="form-control" value="{{ $patient->age }}" name="age" placeholder="{{ trans('main.Age') }}">
                                                            </div>
                                                        </div>
                                                    </div>                                                    

                                                    <div class="col-md-8 offset-md-4">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1"><i class="fa fa-edit"></i>{{ trans('main.Edit Patient') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->            	


            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection