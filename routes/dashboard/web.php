<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// auth secretary
Route::get('/' , 'SecretaryController@login')->name('login');
Route::post('/login/secretary' , 'SecretaryController@loginSecretary')->name('login.secretary');

Route::prefix('dashboard')->middleware('auth')->group(function(){

	// route Language
	Route::get('locale/{locale}' , 'LocaleController@locale')->name('locale');
	// route welcome
	Route::get('welcome' , 'WelcomeController@welcome')->name('welcome');
	// route secretary
	Route::resource('secretary' , 'SecretaryController')->middleware('doctor');
	// route logout secretary
	Route::get('logout' , 'SecretaryController@logout')->name('logout');
	// route patients
	Route::resource('patients' , 'PatientsController');
	Route::get('view/{patient}' , 'PatientsController@view')->name('view');
	// route create booking
	Route::get('booking/patient/{patient}' , 'BookingsController@create')->name('booking.patient');
	// route all bookings
	Route::resource('bookings' , 'BookingsController');
	Route::get('booking/{booking}' , 'BookingsController@bookingDay')->name('booking');
	// route detects
	Route::get('detects' , 'DetectController@index')->name('detects');
	// route create dectect patient
	Route::get('detect/booking/{booking}' , 'DetectController@detect')->name('detect.booking');
	// route store detect patient
	Route::post('detect' , 'DetectController@store')->name('detect');
	// route detect user
	Route::get('detect/patient/{patient}' , 'DetectController@detectPatient')->name('detect.patient');
	// route detect complete
	Route::get('detect/complete/' , 'DetectController@detectComplete')->name('detect.complete');
	//route search code patient
	Route::get('search' , 'PatientsController@search')->name('search');


});
