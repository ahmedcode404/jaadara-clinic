@extends('dashboard.layout.app')
@section('title')
    All Patients
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">


                <!-- BEGIN: add petients -->
                <section id="dashboard-analytics">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-0">{{ trans('main.All Secretary') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="table-responsive mt-1">
                                        <table class="table zero-configuration" id="{{ app()->getLocale() == 'ar' ? 'table_secretary' : '' }}">
                                            <thead>
                                                <tr>

                                                    <th>{{ trans('main.Secretary Name') }}</th>
                                                    <th>{{ trans('main.Email') }}</th>
                                           
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($secretaries as $secretary)

                                                <tr>

                                                    <td>{{ $secretary->name }}</td>
                                                    <td>{{ $secretary->email }}</td>

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- END: add petients -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

@section('scripts')

    <!-- BEGIN: Page Vendor JS-->
    <script src="../../../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="../../../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS--> 

    <script type="text/javascript">
                // translate datatable
            $('#table_secretary').DataTable({
                 
                 "language": 
                        {
                            "sProcessing": "جارٍ التحميل...",
                            "sLengthMenu": "أظهر _MENU_ مدخلات",
                            "sZeroRecords": "لم يعثر على أية سجلات",
                            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                            "sInfoPostFix": "",
                            "sSearch": "ابحث:",
                            "sUrl": "",
                            "oPaginate": {
                                "sFirst": "الأول",
                                "sPrevious": "السابق",
                                "sNext": "التالي",
                                "sLast": "الأخير"
                            }
                        } // end language

                }); // end datatable       
    </script>   

@endsection