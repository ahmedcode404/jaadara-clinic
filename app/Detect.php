<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detect extends Model
{

    protected $fillable = [ 'patient_id' , 'booking_id' , 'diagnosing' , 'anitdotes' , 'rays' ];

    public function booking()
    {
    	return $this->brlongsTo(Booking::class);
    }

    public function patient()
    {
    	return $this->belongsTo(Patient::class , 'patient_id');
    }

    public function images()
    {
    	return $this->hasMany(Image::class , 'detect_id');
    }        
 
}
