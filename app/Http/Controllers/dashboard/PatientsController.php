<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Patient;
class PatientsController extends Controller
{
    public function index(Request $request)
    {

        // session messages success
        if (session()->has('success')) {
            alert()->success(trans('main.Success'),session()->get('success'));
        }

    	$patients = Patient::all();

    	return view('dashboard.patients.index' , compact('patients'));
    } // end function index

    public function create()
    {

    	return view('dashboard.patients.create');

    } // end function create

    public function store(Request $request)
    {
    	$request->validate([
            'name' => 'required|string|min:3',
    		'address' => 'required|min:3',
            'phone' => 'required|max:11|min:11',
    		'age' => 'required',
    	]);

    	$patient = new Patient();
    	$patient->code = rand(10000 , 90000);
        $patient->name = $request->get('name');
    	$patient->address = $request->get('address');
    	$patient->phone = $request->get('phone');
    	$patient->age = $request->get('age');

    	$patient->save();

    	return redirect()->route('patients.index')->with('success' , trans('main.Add Is Successfully'));

    } // end function store

    public function edit(Request $request , Patient $patient)
    {

    	return view('dashboard.patients.edit' , compact('patient'));

    } // end function edit

    public function update(Request $request , Patient $patient)
    {

    	$request->validate([
            'name' => 'required|string|min:3',
    		'address' => 'required|min:3',
    		'phone' => 'required|max:11|min:11',
    		'age' => 'required',
    	]);

    	$patient->code = rand(10000 , 90000);
        $patient->name = $request->get('name');
    	$patient->address = $request->get('address');
    	$patient->phone = $request->get('phone');
    	$patient->age = $request->get('age');

    	$patient->save();

    	return redirect()->route('patients.index')->with('success' , trans('main.Edit Is Successfully'));

    } // end function update

    public function destroy(Patient $patient)
    {

    	$patient->delete();

    	return redirect()->back()->with('success' , trans('main.Delete Is Successfully'));

    } // end function destroy


    public function search(Request $request)
    {
       $patients = Patient::where('code' , $request->get('search'))->get();

       return view('dashboard.patients.search' , compact('patients'));
    }

    public function view(Patient $patient)
    {

       return view('dashboard.patients.view' , compact('patient'));

    }    
}
