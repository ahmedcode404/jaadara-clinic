@extends('dashboard.layout.app')
@section('title')
    Detects
@endsection
<style type="text/css">

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">


             @include('dashboard.layout.messages')

                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        {{ trans('main.Detect For Patient') }}

                                         -

                                        {{ $booking->patient->name }}</h4>

                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{ route('detect') }}" method="post" class="form form-horizontal" enctype="multipart/form-data">
                                        	@csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <input type="hidden" name="booking_id" value="{{ $booking->id }}">
                                                    <input type="hidden" name="patient_id" value="{{ $booking->patient->id }}">


                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Diagnosing') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" id="name" class="form-control pickadate" value="{{ old('diagnosing') }}" name="diagnosing" placeholder="{{ trans('main.Diagnosing') }}">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Anitdotes') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <textarea class="form-control" name="anitdotes">
                                                                    {{ old('anitdotes') }}
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Rays') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                 <input type="file" multiple="multiple" id="files" name="rays[]" class="form-control">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="preview"></div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Other') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <textarea class="form-control" name="other">
                                                                    {{ old('other') }}
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                    </div>                                                    



                                                    <div class="col-md-8 offset-md-4">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1"><i class="fa fa-plus"></i>{{ trans('main.Add Detect') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->


                <!-- BEGIN: latest detect patient -->

                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        @if($latestDetect)
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        {{ trans('main.Latest Detect For Patient') }}

                                         -

                                          {{ $booking->patient->name }}

                                    </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">

                                        <b>{{ trans('main.Diagnosing') }} :

                                        </b>

                                        <span>


                                            {{ $latestDetect->diagnosing }}


                                        </span>

                                        <br><br>

                                        @if($latestDetect->anitdotes)

                                        <b>{{ trans('main.Anitdotes') }} :

                                        </b>


                                         <span>

                                            {{ strip_tags($latestDetect->anitdotes) }}


                                        </span>
                                        @endif

                                        <br><br>
                                        @if($latestDetect->images->count())

                                        <b>{{ trans('main.Rays') }} : </b>
                                        @foreach($latestDetect->images as $image)
                                        <span><img src="{{ url('/images/detects/' . $image->detect_iamges) }}" width="150px" style="margin: 10px;display: inline-block;"></span><br><br>

                                        @endforeach
                                        @endif
                                        @if($latestDetect->other)
                                        <br><br>
                                        <b>{{ trans('main.Other') }} : </b>

                                         <span>

                                            {{ strip_tags($latestDetect->other) }}


                                        </span> 
                                        @endif                                       
                                    </div>
                                </div>
                        <a href="{{ route('detect.patient' , $booking->patient->id) }}" class="btn btn-info">All Detect</a>
                            </div>
                        </div>
                        @endif

                    </div>
                </section>

                <!-- END: latest detect patient -->




            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

@section('scripts')

<script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>

<script type="text/javascript">

    // editor ar detect
    CKEDITOR.replace('anitdotes' );
    // change language from english to arabic
    CKEDITOR.config.language = "{{ app()->getLocale() }}";
    // editor ar detect
    CKEDITOR.replace('anitdotes' );
    // other data
    CKEDITOR.replace('other' );
    


  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img style=\"width:110px;height: 71px;\" class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><button class=\"btn btn-info remove\">Delete</button>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });

        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }



</script>

@endsection
