<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['detect_id' , 'detect_iamges'];

    public function detect()
    {
    	return $this->belongsTo(Detect::class , 'detect_id');
    }
}
