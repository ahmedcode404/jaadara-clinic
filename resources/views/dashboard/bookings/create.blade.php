@extends('dashboard.layout.app')
@section('title')
    Add Booking
@endsection
@section('content')

    <!-- BEGIN: navbar -->

        @include('dashboard.layout.navbar')

    <!-- END: navbar -->


    <!-- BEGIN: sidebar -->

        @include('dashboard.layout.sidebar')

    <!-- END: sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @include('dashboard.layout.messages')
                

                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">

                                        {{ trans('main.Add Booking For') }} 

                                        -


                                            {{ $patient->name }}
    

                                    </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{ route('bookings.store') }}" method="post" class="form form-horizontal">
                                        	@csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <input type="hidden" name="patient_id" value="{{ $patient->id }}">
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Date') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" value="{{ old('date') }}" id="name" class="form-control selector" name="date" placeholder="{{ trans('main.Date') }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Price') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="number" value="{{ old('price') }}" id="price" class="form-control" name="price" placeholder="{{ trans('main.Price') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Type') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select class="form-control" value="{{ old('type') }}" name="type">
                                                                    <option value="detect">{{ trans('main.Detect') }}</option>
                                                                    <option value="operation">{{ trans('main.Operation') }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12" id="price_plus" style="display: none;">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>{{ trans('main.Price Plus Booking') }}</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="number" id="phone" class="form-control" value="{{ old('price_plus') }}" name="price_plus" placeholder="{{ trans('main.Price Plus Booking') }}">
                                                            </div>
                                                        </div>
                                                    </div>                                                                                                        

                                                    <div class="col-md-8 offset-md-4">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1"><i class="fa fa-plus"></i>{{ trans('main.Add Booking') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->            	


            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
        @include('dashboard.layout.footer')
    <!-- END: Footer-->



@endsection

@section('scripts')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ url('app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ url('app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
    <!-- END: Page JS-->
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    

         <script type="text/javascript">
            $(document).ready(function() {

                // when click operation show input price plus 
                $('select[name=type]').on('change', function() {

                    if (this.value == 'operation') {

                      $("#price_plus").show();

                    } else {

                      $("#price_plus").hide();

                    } // end else 

                });

                // glatpickr date and time
                flatpickr(".selector" , {

                    enableTime: true,
                    mitTime: 30,
                    dateFormat: "Y-m-d H:i",
                    locale: "ar",
                    "locale": "ru"

                });


            });


        </script>

@endsection